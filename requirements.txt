certifi==2020.12.5
chardet==4.0.0
click==8.0.0
colorama==0.4.4
dnspython==2.1.0
dominate==2.6.0
email-validator==1.1.2
Flask==2.0.0
Flask-Bootstrap==3.3.7.1
Flask-Login==0.2.11
Flask-OAuth==0.12
Flask-Rauth==0.3.2
Flask-WTF==0.14.3
gunicorn==20.1.0
httplib2==0.19.1
idna==2.10
itsdangerous==2.0.0
Jinja2==3.0.0
MarkupSafe==2.0.0
numpy==1.20.3
oauth2==1.9.0.post1
packaging==20.9
Pillow==8.2.0
PyJWT==2.1.0
pyparsing==2.4.7
python-dateutil==2.8.1
pytz==2021.1
PyYAML==5.4.1
rauth==0.7.3
requests==2.25.1
six==1.16.0
tornado==6.1
typing-extensions==3.10.0.0
urllib3==1.26.4
visitor==0.1.3
Werkzeug==2.0.0
WTForms==2.3.3
