from app import app

from functools import wraps
import requests
from requests.auth import HTTPBasicAuth
from flask import render_template, flash, redirect, url_for, request, make_response
import jwt



class api_call():
    url='https://utilities2021-jjtzu52g4a-lm.a.run.app/api/v1'
    #url='http://127.0.0.1:8080/api/v1'
    @staticmethod
    def get(endpoint="", token=0):
        if token==0: token=request.cookies.get('token_1')
        return requests.get(api_call.url+endpoint,
        headers={'x-access-token':token})

    @staticmethod
    def post(endpoint="", token=0, json={}):
        if token==0: token=request.cookies.get('token_1')
        return requests.post(api_call.url+endpoint, json=json,
        headers={'x-access-token':request.cookies.get('token_1')})

    @staticmethod
    def put(endpoint="", token=0, json={}):
        if token==0: token=request.cookies.get('token_1')
        return requests.put(api_call.url+endpoint, json=json,
        headers={'x-access-token':request.cookies.get('token_1')})

    @staticmethod
    def delete(endpoint, token=0):
        if token==0: token=request.cookies.get('token_1')
        return requests.delete(api_call.url+endpoint,
        headers={'x-access-token':request.cookies.get('token_1')})

    @staticmethod
    def login(username, password):
        api_response=requests.post(api_call.url+'/login', auth=HTTPBasicAuth(username, password))
        return api_response

    @staticmethod
    def reg(json={}):
        return requests.post(api_call.url+'/users', json=json)


    @staticmethod
    def google_reg(json={}):
        return requests.post(api_call.url+'/google_register', json=json)

    @staticmethod
    def google_log(json={}):
        return requests.post(api_call.url+'/google_login', json=json)




def login_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        if not request.cookies.get('token_2'):
            if not request.cookies.get('token_3'):
                return redirect(url_for('logout'))
            #---TOKEN_3
            cook=request.cookies.get('token_3')
            up=jwt.decode(cook,app.config['SECRET_KEY'],algorithms=["HS256"])
            api_response=api_call.login(up['username'],up['password'])
            try:
                token=api_response.json()['token']
                api_response_2=api_call.get('/users',token)
                try:
                    #solving problem, when user was deleted during his own session
                    user = {'username': api_response_2.json()['username']}
                except:
                    return redirect(url_for('logout'))
                response=make_response(f(user,token,*args, **kwargs))
                response.set_cookie('token_1', api_response.json()['token'], max_age=60*30)
                response.set_cookie('token_2', jwt.encode({'username': up['username'], 'password': up['password']}, app.config["SECRET_KEY"], algorithm="HS256"))
                return response
            except:
                flash(api_response.text)
                return redirect(url_for('logout'))
            #---end of TOKEN_3 work
        if not request.cookies.get('token_1'):
            #---TOKEN_2
            cook=request.cookies.get('token_2')
            up=jwt.decode(cook,app.config['SECRET_KEY'],algorithms=["HS256"])
            if up.get('hash')==None:
                api_response=api_call.login(up['username'],up['password'])
            else:
                api_response=api_call.google_log({'hash':up['hash']})

            try:
                token=api_response.json()['token']
                api_response_2=api_call.get('/users',token)
                try:
                    #solving problem, when user was deleted during his own session
                    user = {'username': api_response_2.json()['username']}
                except:
                    return redirect(url_for('logout'))
                response=make_response(f(user,token,*args, **kwargs))
                response.set_cookie('token_1', api_response.json()['token'], max_age=60*30)
                return response
            except:
                flash(api_response.text)
                return redirect(url_for('logout'))
            #---end of TOKEN_2 work
        #TOKEN_1
        api_response=api_call.get('/users')
        try:
            #solving problem, when user was deleted during his own session
            user = {'username': api_response.json()['username']}
        except:
            return redirect(url_for('logout'))
        token=0
        return f(user,token, *args, **kwargs)
    return decorator
