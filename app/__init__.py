from flask import Flask
from flask_bootstrap import Bootstrap
from flask_wtf import CsrfProtect

csrf = CsrfProtect()
app = Flask(__name__)
app.config['SECRET_KEY'] = 'gakumba'
app.config['GOOGLE_LOGIN_CLIENT_ID'] = "1001152211416-dc1llrs2s1rfgttumcqf4hsorl5ntuj2.apps.googleusercontent.com"
#app.config['GOOGLE_LOGIN_CLIENT_SECRET'] = "lXPz4tazGoRWYv_RWYLNAOqZ"
app.config['OAUTH_CREDENTIALS'] = {
        'google': {
            'id': "1001152211416-dc1llrs2s1rfgttumcqf4hsorl5ntuj2.apps.googleusercontent.com",
            'secret': "lXPz4tazGoRWYv_RWYLNAOqZ"
        }
}




bootstrap = Bootstrap(app)
csrf.init_app(app)

from app import routes, errors
