from app import app
from app.oauth import *
from app.forms import *
from app.tools import *
from app.errors import *
from flask import render_template, flash, redirect, url_for, request, make_response
from datetime import datetime,date, timedelta
from calendar import monthrange
import jwt

@app.route('/index')
def index():
    resp=api_call.get()
    return resp.json()


@app.route('/login', methods= ['GET', 'POST'])
def login():
    if request.cookies.get('token_1'):
        return redirect(url_for('main'))
    form = Login_form()
    if form.validate_on_submit():
        api_response=api_call.login(form.username.data,form.password.data)
        try:
            response=make_response(redirect(url_for('main')))
            response.set_cookie('token_1', api_response.json()['token'], max_age=60*30)
            response.set_cookie('token_2', jwt.encode({'username': form.username.data, 'password': form.password.data}, app.config["SECRET_KEY"], algorithm="HS256"))
            if form.remember_me.data:
                response.set_cookie('token_3', jwt.encode({'username': form.username.data, 'password': form.password.data}, app.config["SECRET_KEY"], algorithm="HS256"),max_age=60*60*24*365)
            return response
        except:
            flash(api_response.text)
            return redirect(url_for('login'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/oauth_login/<prv>')
def oauth_login(prv):
    if request.cookies.get('token_1'):
        return redirect(url_for('main'))
    oauth = OAuthSignIn.get_provider(prv)
    return oauth.authorize()


@app.route('/oauth_callback/<prv>')
def oauth_callback(prv):
    oauth = OAuthSignIn.get_provider(prv)
    if request.cookies.get('token_1'):
        return redirect(url_for('main'))
    oauth = OAuthSignIn.get_provider(prv)
    user=oauth.callback()
    if not user:
        flash('Authentication failed.')
        return redirect(url_for('login'))

    username=user['email'].split('@')[0]
    email=user['email']
    hash=jwt.encode({'username': username, 'email': email}, 'Yog-Sothoth', algorithm="HS256")
    api_response=api_call.google_log({'hash': hash})
    if api_response.status_code==400:
        flash(api_response.text)
        return redirect(url_for('login'))
    if api_response.status_code==404:
        hash=jwt.encode({'username': username, 'email': email}, 'Nyarlathotep', algorithm="HS256")
        api_response=api_call.google_reg({'hash': hash})
        if api_response.status_code==400:
            flash(api_response.text)
            return redirect(url_for('login'))
    api_token=api_response.json()['token']

    hash=jwt.encode({'username': username, 'email': email}, 'Yog-Sothoth', algorithm="HS256")
    response=make_response(redirect(url_for('main')))
    response.set_cookie('token_1', api_token, max_age=60*30)
    response.set_cookie('token_2', jwt.encode({'hash': hash}, app.config["SECRET_KEY"], algorithm="HS256"))
    return response





@app.route('/logout')
def logout():
    response=make_response(redirect(url_for('login')))
    response.set_cookie('token_1', "", max_age=0)
    response.set_cookie('token_2', "", max_age=0)
    response.set_cookie('token_3', "", max_age=0)
    return response


@app.route('/register', methods=['GET','POST'])
def register():
    if request.cookies.get('token_1'):
        return redirect(url_for('main'))
    form= Registration_form()
    if form.validate_on_submit():
        js={}
        js['username']=form.username.data
        js['email']=form.email.data
        js['password']=form.password.data
        print(js)
        api_response=api_call.reg(js)
        if api_response.status_code==400:
            flash(api_response.text)
            return redirect(url_for('register'))
        if api_response.status_code==200:
            flash("Registered successfully!")
            return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/user')
@login_required
def user(user,token):
    return user['username']


@app.route('/')
@app.route('/main')
@login_required
def main(user,token):
    api_response=api_call.get('/apartments',token)
    try:
        apartments = api_response.json()['apartments']
    except:
        return api_response.text
    return render_template('apartments.html', title='Apartments',user=user, apartments=apartments)


@app.route('/apartments/<aid>')
@login_required
def apartments(user,token,aid):
    api_response=api_call.get('/apartments/{}'.format(aid),token)
    apart=api_response.json()
    summary=api_response.json()['summary']
    api_response=api_call.get('/apartments/{}/counters'.format(aid),token)
    counters=api_response.json()['counters']
    api_response=api_call.get('/apartments/{}/payments'.format(aid),token)
    payments=api_response.json()['payments']
    return render_template('apart.html', title='Apartment',apart=apart, counters=counters, payments=payments,summary=summary, aid=aid)



@app.route('/submit_apartment/<aid>', methods=['GET','POST'])
@login_required
def submit_apart(user,token, aid):
    api_response=api_call.get('/apartments/{}/counters'.format(aid),token)
    counters=api_response.json()['counters']
    clen=len(counters)
    forms=[]
    for c in counters:
        forms.append({'id':c['id'],'name': c['name'],'custom_timestamp': datetime.now()})
    form=submit_apart_form(counters=forms)
    if form.validate_on_submit():
        for c in form.counters.data:
            print(c)
            api_response=api_call.post('/apartments/{}/counters/{}/values'.format(aid,c['id']), token ,{'value':float(c['value']),'timestamp':c['custom_timestamp'].isoformat()})
            if api_response.status_code!=201:
                flash(api_response.text)
                return redirect(url_for('submit_apart',aid=aid))
        return redirect(url_for('apartments',aid=aid))
    return render_template('submit.html', title='Submit all meters', counters=counters, form=form, clen=clen)

@app.route('/forecast/<aid>', methods=['GET','POST'])
@login_required
def forecast(user,token, aid):
    api_response=api_call.get('/apartments/{}/counters'.format(aid),token)
    counters=api_response.json()['counters']
    clen=len(counters)
    forms=[]
    for c in counters:
        forms.append({'id':c['id'],'name': c['name'],'custom_timestamp': datetime.now()})
    form=submit_apart_form(counters=forms)
    if form.validate_on_submit():
        apart=api_call.get('/apartments/{}'.format(aid), token).json()
        summary={}
        summary['counters']=[]
        summary['sum']=0
        for c in form.counters.data:
            api_response=api_call.get('/apartments/{}/counters/{}'.format(aid,c['id']), token)
            js=api_response.json()
            if not js['last_value']:
                flash('some of counters is not previously submitted')
                return redirect(url_for('forecast', aid=aid))
            cost=js['cost']
            limit=js['limit']
            second_cost=js['second_cost']
            value2=float(c['value'])
            value1=js['last_value']
            time2=c['custom_timestamp']
            time1=datetime.strptime(js['last_time'], "%m/%d/%Y").date()
            days_in_month= monthrange(time1.year, time1.month)[1]
            forecast_date=time1 + timedelta(days=days_in_month)
            days=(time2-time1).days
            if days<=0 or days>=days_in_month:
                flash('some of timestamps incorrect for forecast')
                return redirect(url_for('forecast', aid=aid))
            diff=value2-value1
            if diff<=0:
                flash('some of values incorrect for forecast')
                return redirect(url_for('forecast', aid=aid))
            average=diff/days
            forecast_value=average*days_in_month
            if limit==0 or forecast_value<=limit:
                forecast_cost=forecast_value*cost
            else:
                forecast_cost=limit*cost+(forecast_value-limit)*second_cost
            summary['counters'].append({'name':js['name'],
                                        'avr':average,
                                        'days_passed':days,
                                        'avr_in_currency':forecast_cost/days_in_month,
                                        'forecast_date':forecast_date.strftime("%m/%d/%Y"),
                                        'forecast_value':forecast_value,
                                        'forecast_cost':forecast_cost
                                        })
            summary['sum']+=forecast_value*cost
        return render_template('forecast.html', title="Forecast", summary=summary, aid=aid, apart=apart)
    return render_template('submit.html', title='Make forecast', counters=counters, form=form, clen=clen)



@app.route('/new_apartment', methods=['GET','POST'])
@login_required
def new_apart(user,token):
    form=edit_address_form()
    if form.validate_on_submit():
        api_response=api_call.post('/apartments',token,{'address':form.address.data})
        if api_response.status_code==201:
            flash('Address created!')
            return redirect(url_for('main'))
        else:
            flash(api_response.text)
            return redirect(url_for('new_apart'))
    return render_template('edit_address.html', title='New address', form=form)


@app.route('/apartments/<aid>/<path1>', methods=['GET','POST'])
@login_required
def apart_manipul(user,token,aid, path1):
    if path1=='edit':
        apart=api_call.get('/apartments/{}'.format(aid),token)
        form=edit_address_form(address=apart.json()['address'])
        if form.validate_on_submit():
            api_response=api_call.put('/apartments/{}'.format(aid),token,{'address':form.address.data})
            if api_response.status_code==200:
                flash('Address changed!')
                return redirect(url_for('apartments',aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('apart_manipul',aid=aid, path1='edit'))
        return render_template('edit_address.html', title='Edit address', form=form, aid=aid)
    if path1=='delete':
        form=delete_form()
        if form.validate_on_submit():
            api_response=api_call.delete('/apartments/{}'.format(aid),token)
            if api_response.status_code==200:
                flash('Address deleted!')
                return redirect(url_for('main'))
            else:
                flash(api_response.text)
                return redirect(url_for('apart_manipul',aid=aid, path1='delete'))
        return render_template('delete.html', title='Delete address', form=form, aid=aid)


@app.route('/counters/<aid>/<cid>')
@login_required
def counters(user,token, aid, cid):
    api_request=api_call.get('/apartments/{}/counters/{}'.format(aid,cid),token)
    counter=api_request.json()
    api_request=api_call.get('/apartments/{}/counters/{}/values'.format(aid,cid),token)
    values=api_request.json()['values']
    return render_template('counter.html', aid=aid,cid=cid, counter=counter,title='Counter',values=values)


@app.route('/new_counter/<aid>', methods=['GET','POST'])
@login_required
def new_counter(user,token, aid):
    form=new_counter_form(custom_timestamp=datetime.now())
    if form.validate_on_submit():
        js={}
        js['name']=form.name.data
        js['cost']=float(form.cost.data)
        if form.limit.data and form.second_cost.data:
            js['limit']=float(form.limit.data)
            js['second_cost']=float(form.second_cost.data)
        api_response=api_call.post('/apartments/{}/counters'.format(aid),token,js)
        if api_response.status_code==201:
            flash('Counter created!')
            cid=api_response.json()['id']
            js['value']=float(form.init_value.data)
            js['timestamp']=form.custom_timestamp.data.isoformat()
            api_response=api_call.post('/apartments/{}/counters/{}/values'.format(aid,cid),token,js)
            if api_response.status_code==201:
                flash('Value added!')
                return redirect(url_for('apartments', aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('new_counter',aid=aid))
        else:
            flash(api_response.text)
            return redirect(url_for('new_counter',aid=aid))
    return render_template('edit_counter.html', title='New counter', form=form)


@app.route('/counters/<aid>/<cid>/<path1>', methods=['GET','POST'])
@login_required
def counter_manipul(user,token, aid, cid, path1):
    if path1=='edit':
        counter=api_call.get('/apartments/{}/counters/{}'.format(aid,cid),token)
        cd=counter.json()
        form=edit_counter_form(cost=cd['cost'],name=cd['name'], limit=cd['limit'],second_cost=cd['second_cost'])
        if form.validate_on_submit():
            js={}
            if form.name.data and form.name.data!=cd['name']:
                js['name']=form.name.data
            if form.cost.data and form.cost.data!=cd['cost']:
                js['cost']=float(form.cost.data)
            if form.limit.data and form.limit.data!=cd['limit']:
                js['limit']=float(form.limit.data)
            if form.second_cost.data and form.second_cost.data!=cd['second_cost']:
                js['second_cost']=float(form.second_cost.data)
            api_response=api_call.put('/apartments/{}/counters/{}'.format(aid,cid),token,js)
            if api_response.status_code==200:
                flash(api_response.text)
                return redirect(url_for('apartments', aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('counter_manipul',aid=aid, cid=cid, path1=path1))
        return render_template('edit_counter.html', title='Edit counter', form=form)

    if path1=='delete':
        form=delete_form()
        if form.validate_on_submit():
            api_response=api_call.delete('/apartments/{}/counters/{}'.format(aid,cid),token)
            if api_response.status_code==200:
                flash(api_response.text)
                return redirect(url_for('apartments', aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('counter_manipul',aid=aid, cid=cid, path1=path1))
        return render_template('delete.html', title='Delete counter', form=form, aid=aid)


@app.route('/payments/<aid>/<cid>')
@login_required
def payments(user,token, aid, cid):
    api_request=api_call.get('/apartments/{}/payments/{}'.format(aid,cid),token)
    payment=api_request.json()
    return render_template('payment.html', aid=aid,cid=cid, payment=payment,title='Payment')


@app.route('/new_payment/<aid>', methods=['GET','POST'])
@login_required
def new_payment(user,token, aid):
    form=edit_payment_form(custom_timestamp=datetime.now())
    if form.validate_on_submit():
        js={}
        js['name']=form.name.data
        js['value']=float(form.value.data)
        api_response=api_call.post('/apartments/{}/payments'.format(aid),token,js)
        if api_response.status_code==201:
            flash('Payment created!')
            return redirect(url_for('apartments',aid=aid))
        else:
            flash(api_response.text)
            return redirect(url_for('new_payment',aid=aid))
    return render_template('edit_counter.html', title='New payment', form=form)


@app.route('/payments/<aid>/<cid>/<path1>', methods=['GET','POST'])
@login_required
def payment_manipul(user,token, aid, cid, path1):
    if path1=='edit':
        payment=api_call.get('/apartments/{}/payments/{}'.format(aid,cid),token)
        cd=payment.json()
        form=edit_payment_form(value=cd['value'],name=cd['name'])
        if form.validate_on_submit():
            js={}
            if form.name.data and form.name.data!=cd['name']:
                js['name']=form.name.data
            if form.value.data and form.value.data!=cd['value']:
                js['value']=float(form.value.data)
            api_response=api_call.put('/apartments/{}/payments/{}'.format(aid,cid),token,js)
            if api_response.status_code==200:
                flash(api_response.text)
                return redirect(url_for('apartments', aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('payment_manipul',aid=aid, cid=cid, path1=path1))
        return render_template('edit_counter.html', title='Edit payment', form=form)

    if path1=='delete':
        form=delete_form()
        if form.validate_on_submit():
            api_response=api_call.delete('/apartments/{}/payments/{}'.format(aid,cid),token)
            if api_response.status_code==200:
                flash(api_response.text)
                return redirect(url_for('apartments', aid=aid))
            else:
                flash(api_response.text)
                return redirect(url_for('payment_manipul',aid=aid, cid=cid, path1=path1))
        return render_template('delete.html', title='Delete payment', form=form, aid=aid)


@app.route('/add_value/<aid>/<cid>', methods=['GET','POST'])
@login_required
def add_value(user,token,aid,cid):
    form=add_value_form(custom_timestamp=datetime.now())
    if form.validate_on_submit():
        js={}
        js['value']=float(form.value.data)
        js['timestamp']=form.custom_timestamp.data.isoformat()
        api_response=api_call.post('/apartments/{}/counters/{}/values'.format(aid,cid),token,js)
        if api_response.status_code==201:
            flash('Value added!')
            return redirect(url_for('counters', aid=aid, cid=cid))
        else:
            flash(api_response.text)
            return redirect(url_for('add_value',aid=aid, cid=cid))
    return render_template('add_value.html', title='Add value', form=form, aid=aid, cid=cid)


@app.route('/values/<aid>/<cid>/<vid>/<path1>', methods=['GET','POST'])
@login_required
def values_manipul(user,token,aid,cid,vid,path1):
    if path1=='edit':
        try:
            val=api_call.get('/apartments/{}/counters/{}/values/{}'.format(aid,cid,vid),token)
            vald=val.json()
        except:
            flash(api_response.text)
            return redirect(url_for('values_manipul',aid=aid, cid=cid, vid=vid, path1=path1))
        form=edit_value_form(value=vald['value'])
        if form.validate_on_submit():
            api_response=api_call.put('/apartments/{}/counters/{}/values/{}'.format(aid,cid,vid),token,{"value":float(form.value.data)})
            if api_response.status_code==200:
                flash('Value changed!')
                return redirect(url_for('counters', aid=aid, cid=cid))
            else:
                flash(api_response.text)
                return redirect(url_for('values_manipul',aid=aid, cid=cid, vid=vid, path1=path1))
        return render_template('add_value.html', title='Change value', form=form, aid=aid, cid=cid)

    if path1=='delete':
        form=delete_form()
        if form.validate_on_submit():
            api_response=api_call.delete('/apartments/{}/counters/{}/values/{}'.format(aid,cid,vid),token)
            if api_response.status_code==200:
                flash(api_response.text)
                return redirect(url_for('counters',aid=aid,cid=cid))
            else:
                flash(api_response.text)
                return redirect(url_for('values_manipul',aid=aid, cid=cid, vid=vid, path1=path1))
        return render_template('delete.html', title='Delete value', form=form, aid=aid)
