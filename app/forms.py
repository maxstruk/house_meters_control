from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DateField, HiddenField, FieldList, FormField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo

class Login_form(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Pasword', validators=[DataRequired()])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Sign In')

class Registration_form(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')


class edit_address_form(FlaskForm):
    address = StringField('New address', validators=[DataRequired()])
    submit=SubmitField('Submit')

class new_counter_form(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    cost = StringField('Cost', validators=[DataRequired()])
    limit = StringField('Limit (optional)')
    second_cost = StringField('Second cost (optional)')
    init_value = StringField('Initial value', validators=[DataRequired()])
    custom_timestamp= DateField('custom timestamp (month-day-year)',format='%m-%d-%Y')
    submit=SubmitField('Submit')

    def validate_cost(self, cost):
        try: float(cost.data)
        except:
            raise ValidationError('Please, enter a number')
    def validate_limit(self, limit):
        if limit.data:
            try: float(limit.data)
            except:
                raise ValidationError('Please, enter a number')
    def validate_second_cost(self, second_cost):
        if second_cost.data:
            try: float(second_cost.data)
            except:
                raise ValidationError('Please, enter a number')

    def validate_init_value(self, init_value):
        try: float(init_value.data)
        except:
            raise ValidationError('Please, enter a number')

class edit_counter_form(FlaskForm):
    name = StringField('Name')
    cost = StringField('Cost')
    limit = StringField('Limit')
    second_cost = StringField('Second cost')
    submit=SubmitField('Submit')
    def validate_cost(self, cost):
        if cost.data:
            try: float(cost.data)
            except:
                raise ValidationError('Please, enter a number')
    def validate_limit(self, limit):
        if limit.data:
            try: float(limit.data)
            except:
                raise ValidationError('Please, enter a number')
    def validate_second_cost(self, second_cost):
        if second_cost.data:
            try: float(second_cost.data)
            except:
                raise ValidationError('Please, enter a number')

class edit_payment_form(FlaskForm):
    name = StringField('Name', validators=[DataRequired()])
    value = StringField('Value', validators=[DataRequired()])
    submit=SubmitField('Submit')
    def validate_value(self, value):
        try: float(value.data)
        except:
            raise ValidationError('Please, enter a number')

class delete_form(FlaskForm):
    submit=SubmitField('DELETE')

class add_value_form(FlaskForm):
    value = StringField('Add value', validators=[DataRequired()])
    custom_timestamp= DateField('custom timestamp(month-day-year)',format='%m-%d-%Y')
    submit=SubmitField('Submit')
    def validate_value(self, value):
        try: float(value.data)
        except:
            raise ValidationError('Please, enter a number')

class edit_value_form(FlaskForm):
    value = StringField('Edit value', validators=[DataRequired()])
    submit=SubmitField('Submit')
    def validate_value(self, value):
        try: float(value.data)
        except:
            raise ValidationError('Please, enter a number')

class counter_for_submit_form(FlaskForm):
    name= HiddenField('name', validators=[DataRequired()])
    id= HiddenField('id', validators=[DataRequired()])
    value = StringField('Add value', validators=[DataRequired()])
    custom_timestamp= DateField('custom timestamp(month-day-year)',format='%m-%d-%Y')
    def validate_value(self, value):
        try: float(value.data)
        except:
            raise ValidationError('Please, enter a number')

class submit_apart_form(FlaskForm):
    counters= FieldList(FormField(counter_for_submit_form), min_entries=0)
    submit=SubmitField('Submit')
